# Databricks notebook source
# MAGIC %md
# MAGIC #Biblioteki

# COMMAND ----------

!pip install nltk
!pip install morfeusz2
!pip install pystempel
!pip install imblearn
!pip install keras
!pip install tensorflow


# COMMAND ----------

from pyspark.sql.functions import lower,regexp_replace,trim,col,monotonically_increasing_id,explode,regexp_extract,rand
from pyspark.ml.feature import Tokenizer, CountVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split,GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, roc_curve, auc
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import SnowballStemmer
from morfeusz2 import Morfeusz
from stempel import StempelStemmer
import re
from imblearn.over_sampling import SMOTE
import nltk
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

nltk.download('punkt')

# COMMAND ----------

# MAGIC %md
# MAGIC #Wczytanie danych

# COMMAND ----------

df_target = spark.read.text("/mnt/datascience/tomek_stuff/hate_speech/dataset_tags.txt").withColumnRenamed("value", "target").withColumn("index", monotonically_increasing_id())
df_var = spark.read.text("/mnt/datascience/tomek_stuff/hate_speech/dataset_texts.txt").withColumnRenamed("value", "text").withColumn("index", monotonically_increasing_id())
df_pl_stopwords = spark.read.text("/mnt/datascience/tomek_stuff/hate_speech/polish.stopwords.txt").withColumnRenamed("value", "stopwords") #https://github.com/bieli/stopwords/blob/master/polish.stopwords.txt

# COMMAND ----------

base_df = df_target.join(df_var, "index").drop("index")
base_df = base_df.withColumn("target", col("target").cast("int"))
base_df.show()

# COMMAND ----------

base_df.describe().show()
base_df.select("target").printSchema()
base_df.select("text").printSchema()

# COMMAND ----------

# MAGIC %md
# MAGIC #Podstawowe czyszczenie danych

# COMMAND ----------

edit_df = base_df.withColumn("text", 
                             lower(
                                 trim(
                                     regexp_replace(
                                         regexp_replace(
                                             regexp_replace(
                                                 regexp_replace(
                                                     regexp_replace(
                                                         base_df["text"], 
                                                         "[\p{Punct}]", ""     # znaki interpunkcji
                                                     ), 
                                                     "anonymizedaccount", ""   # zanonimizowane nazwy użytkowników 
                                                 ), 
                                                 "<[^>]+>", ""                 # tagi HTML  
                                             ), 
                                             "\s+", " "                        # wielokrotne białe znaki
                                         ), 
                                         "\t", ""                              # tabulatory   
                                     )
                                 )
                             )
                            )


edit_df = edit_df.withColumn("text", regexp_replace(edit_df["text"], ":\)|;\)|:-\)|:-D|:-\(|:\(|:-\(|😂|😉", "")) # emotikony

# COMMAND ----------

emoticons_df = edit_df.select(regexp_extract(edit_df["text"], ":\)|;\)|:-\)|:-D|:-\(|:\(|:-\(|😂|😉", 0).alias("emoticon"))

emoticons = emoticons_df.filter(emoticons_df["emoticon"] != "").select("emoticon").distinct()

emoticons.show()

if emoticons.count() == 0:
    print("Nie ma już emotikon w tekście.")
else:
    print("Emotikony wciąż istnieją w tekście.")

# COMMAND ----------

edit_df.sample(fraction=0.10).show()

# COMMAND ----------

# MAGIC %md
# MAGIC #Analiza

# COMMAND ----------

pandas_df = edit_df.toPandas()

class_distribution = pandas_df['target'].value_counts()

class_distribution.plot(kind='bar', color=['blue', 'red'])
plt.title('Rozkład klas')
plt.xlabel('Klasa')
plt.ylabel('Liczba obserwacji')
plt.xticks(rotation=0)
plt.show()

text_length = pandas_df['text'].apply(len)

plt.hist(text_length, color='skyblue', bins=50)
plt.title('Histogram długości tekstów')
plt.xlabel('Długość tekstu')
plt.ylabel('Liczba obserwacji')
plt.show()

# COMMAND ----------

plt.figure(figsize=(10, 6))

plt.hist(pandas_df[pandas_df['target'] == 0]['text'].apply(len), color='blue', alpha=0.5, bins=50, label='Klasa 0')

plt.hist(pandas_df[pandas_df['target'] == 1]['text'].apply(len), color='red', alpha=0.5, bins=50, label='Klasa 1')

plt.title('Histogram długości tekstów dla każdej klasy')
plt.xlabel('Długość tekstu')
plt.ylabel('Liczba obserwacji')
plt.legend()
plt.show()


# COMMAND ----------

empty_text_records = pandas_df[pandas_df['text'].isnull() | (pandas_df['text'] == '')].shape[0]
print("Liczba rekordów z pustą wartością w kolumnie 'text':", empty_text_records)

# COMMAND ----------

unique_values_class_0 = pandas_df[pandas_df['target'] == 0]['text'].value_counts()
unique_values_class_1 = pandas_df[pandas_df['target'] == 1]['text'].value_counts()

print("Unikatowe wartości dla klasy 0:")
print(unique_values_class_0[:5])

print("\nUnikatowe wartości dla klasy 1:")
print(unique_values_class_1[:5])

# COMMAND ----------

pandas_df_pl_stopwords = df_pl_stopwords.toPandas()

for klasa in [1,0]:
    all_text = ' '.join(pandas_df[pandas_df['target'] == klasa]['text'])
    words = all_text.split()
    stopwords_set = set(pandas_df_pl_stopwords['stopwords'])
    filtered_words = [word for word in words if word not in stopwords_set]
    word_counts = pd.Series(filtered_words).value_counts()
    print(f"Najczęściej występujące słowa w klasie {klasa}:")
    print(word_counts.head(10))
    print()


# COMMAND ----------

# MAGIC %md
# MAGIC #Dodatkowe czyszczenie danych 

# COMMAND ----------

df_clean=pandas_df
df_clean = df_clean[df_clean['text'] != 'name']
df_clean['text'] = df_clean['text'].str.replace('rt', '')

# COMMAND ----------

# MAGIC %md
# MAGIC #Tokenizacja, Lemantyzacja, Stop words

# COMMAND ----------

stemmer = StempelStemmer.polimorf()

def tokenizer(text):
    tokens = word_tokenize(text)
    stopwords_set = set(pandas_df_pl_stopwords['stopwords'])
    tokens = [stemmer.stem(word) for word in tokens if word is not None and word not in stopwords_set]
    tokens = [word for word in tokens if word is not None]
    return ' '.join(tokens)

df_clean['clean_text'] = df_clean['text'].apply(tokenizer)

# COMMAND ----------

# MAGIC %md
# MAGIC #Wektoryzacja tekstu

# COMMAND ----------

vectorizer = CountVectorizer()
X = vectorizer.fit_transform(df_clean['clean_text'])
y = df_clean['target']

# COMMAND ----------

# MAGIC %md
# MAGIC #Modelowanie

# COMMAND ----------

# MAGIC %md
# MAGIC ##Podział na set testowy i treningowy

# COMMAND ----------

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# COMMAND ----------

# MAGIC %md
# MAGIC ##Zbalansowanie zbiorów

# COMMAND ----------

smote = SMOTE() # generowanie szucznych rekordów dla mniejszej klasy 
X_train_balanced, y_train_balanced = smote.fit_resample(X_train, y_train)

# COMMAND ----------

# MAGIC %md
# MAGIC ##Tworzenie modeli

# COMMAND ----------

models=[]

# COMMAND ----------

model_NB = MultinomialNB()
model_NB.fit(X_train_balanced, y_train_balanced)
models.append(model_NB)

# COMMAND ----------

model_LR = LogisticRegression()
model_LR .fit(X_train_balanced, y_train_balanced)
models.append(model_LR)

# COMMAND ----------

model_SVC = SVC()
model_SVC.fit(X_train_balanced, y_train_balanced)
models.append(model_SVC)

# COMMAND ----------

model_RF = RandomForestClassifier()
model_RF.fit(X_train_balanced, y_train_balanced)
models.append(model_RF)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Ocena modeli

# COMMAND ----------

def models_report(model):
    print(f"{type(model).__name__}")
    y_pred = model.predict(X_test)
    print(classification_report(y_test, y_pred))
    print()   

# COMMAND ----------

for model in models:
    models_report(model) 


# COMMAND ----------

def models_ROC(model):
    y_pred = model.predict(X_test)

    fpr, tpr, thresholds = roc_curve(y_test, y_pred)
    roc_auc = auc(fpr, tpr)

    plt.figure()
    plt.plot(fpr, tpr, color='darkorange', lw=2, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(f'{type(model).__name__} (ROC)')
    plt.legend(loc="lower right")
    plt.show()

# COMMAND ----------

for model in models:
    models_ROC(model)

# COMMAND ----------

# MAGIC %md
# MAGIC ##Wybór najlepszego modelu i optymalizacja hiperparametrów

# COMMAND ----------

param_grid = {
    'alpha': np.arange(0.1, 2.0, 0.1), 
    'fit_prior': [True, False]
}

model_NB = MultinomialNB()

grid_search = GridSearchCV(model_NB, param_grid, cv=5)

grid_search.fit(X_train_balanced, y_train_balanced)


print("Najlepsze parametry:", grid_search.best_params_)
print("Najlepszy wynik walidacji krzyżowej:", grid_search.best_score_)

best_model = grid_search.best_estimator_

# COMMAND ----------

models_report(best_model) 

# COMMAND ----------

models_ROC(best_model)

# COMMAND ----------

# MAGIC %md
# MAGIC #Podsumowanie

# COMMAND ----------

# MAGIC %md
# MAGIC Powyżej mamy notatnik, który ma za zadanie przewidywać czy dany wpis jest obraźliwy czy neutralny.
# MAGIC
# MAGIC Z analizy wynika, że zbiory klas są bardzo nie zbilansowane (jeśli chodzi o ekspozycje) oraz długości tekstów w każdej z klas maja zupełnie inne rozkłady. Brak jest rekordów pustych, nie występuję dużo duplikatów oraz zdecydowana największa powtarzalność występowania słów ma swoje odzwierciedlenie w języku polskim po za jednym błędem „rt”. W wielu rekordach występowała zanimizowana nazwa użytkowania „@anonymizeadaccount”. Dodatkowo teksty szczególnie klasy neutralnej posiadają wiele emotikonek.
# MAGIC
# MAGIC Po oczyszczeniu danych (pozbyciu się emotikonek, interpunkcji, anonimizacji, tagów, białych znaków, stop words, anomalii oraz zmniejszenie liter) i odpowiednim ich przetworzeniu (lemantyzacja, tokenizacja, wektoryzacja do reprezentacji numerowej, zbalansowanie zbioru) zostały stworzone różne modele na domyślnych parametrach do przeprowadzenia klasyfikacji. Po wyborze najlepszego modelu na podstawie metrykach opartych na macierzy pomyłek czyli Naiwnego Bayaes’a, została przeprowadzona optymalizacja owego modelu poprzez dobór najlepszych parametrów i zastosowaniu walidacji krzyżowej.
# MAGIC
# MAGIC Finalny model radzi sobie bardzo dobrze z przewidywaniem neutralnych wpisów gorzej wychodzi mu przewidywanie wpisów obraźliwych. Ogólny scoring modelu jest nie najgorszy bo wychodzi 0,73 AUC i ma dobry performance. 
# MAGIC Z dodatkowych rzeczy jakie można przetestować lub zrobić w przyszłości to zasilenie zbioru większą liczbą danych szczególnie wpisów obraźliwych. Spróbować wykorzystać techniki sieci generatywnych przeciwników (GAN) lub Word2Vec do wygenerowania większej liczby danych na podstawie już istniejących danych. Przy odpowiednio dużej liczbie danych przeprowadzić próbę stworzenia modelu klasyfikacji za pomocą sieci neuronowych.
# MAGIC
# MAGIC PS. Przy aktualnym zbiorze danych zauważyłem, że pozostawienie emotikonek poprawiało AUC bo gównie występowały w klasie neutralnej. Ale postanowiłem się ich pozbyć bo przynosiło to zagrożenie, że przy ewentualnych nowych danych do predykcji wpisy faktycznie obraźliwe mogłyby zawierać właśnie emotikonki. Chociaż może pomogłaby się przed tym zabezpieczyć biblioteka Great Expectations. 
# MAGIC
